extends Node


@onready var GRAVITY_CONSTANT = %Game.GRAVITY_CONSTANT

var random



var celestial_body_scene = preload("res://celestial_body.tscn")
func generate_galaxy():
	var position = Vector2.ZERO
	var velocity = Vector2.ZERO
	generate_system(3, position, velocity)
	pass
	
func generate_system(level, position, velocity):
#	print("system")
#	print(level)
#	print(position)
#	print(velocity)
	var mass
	var radius
	var next_period
	var num_satellites
	
	if level == 3:
		mass = 150000
		radius = 27
		next_period = 35 * 60
		num_satellites = 16
	if level == 2:
		mass = 1000
		radius = 9 
		next_period = 4 * 60
		num_satellites = 4
	if level == 1: 
		mass = 5 
		radius = 1
		next_period = NAN
	
	generate_body(mass, position, velocity, "aaa", radius, 3 - level)
	
	if level > 1:
		
		for i in range(0,num_satellites):
			var variation = 1 + (random.randf() * 0.6 - 0.3)
			next_period = next_period * variation
			var next_orbit_distance : float = calc_distance(mass, next_period)
			var next_direction : Vector2 = Vector2(1,0).rotated(random.randf() * PI * 2)
			var next_position : Vector2 = position + next_direction * next_orbit_distance
			
			var next_velocity_abs = calc_velocity(mass, next_orbit_distance)
			var next_velocity_direction = next_direction.orthogonal()
			var next_velocity = velocity + next_velocity_direction * next_velocity_abs
			
			generate_system(level - 1, next_position, next_velocity)
	
	
	
	

func generate_body(mass, position, velocity, body_name, radius, sprite):
	var main_body : CelestialBody= celestial_body_scene.instantiate()
	main_body.name = body_name
	main_body.mass = mass
	main_body.position = position
	main_body.initial_velocity = velocity
	main_body.radius = radius
	main_body.sprite_number = sprite
	%Bodies.add_child(main_body)

func _ready():
	random = RandomNumberGenerator.new()
	random.randomize()
	generate_galaxy()



func calc_distance(M : float,T : float) -> float:
	var mu : float = sqrt(GRAVITY_CONSTANT * M) 
	var PI2 : float = 2 * PI
	var R : float = 1 / ((PI2 / (T * mu)) ** (0.66666))
	return R
	
func calc_velocity(M : float, R : float) -> float:
	return sqrt((GRAVITY_CONSTANT * M) / R)





	
func hardcoded_system():
	# black hole
	generate_body(150000, Vector2.ZERO, Vector2.ZERO, "black hole", 5, 6)
	# sun 1
	generate_body(1000, Vector2(0,20000), Vector2(86.602, 0), "sun", 5, 5)
	# planet 1
	generate_body(10, Vector2(1000,20000), Vector2(86.602, 31.623), "earth", 2, 2)

func hardcoded_system_1():
	generate_body(150000, Vector2.ZERO, Vector2.ZERO, "black hole", 5, 6)
	generate_body(1000, Vector2(0,20000), Vector2(86.602, 0), "sun", 5, 5)
	generate_body(10, Vector2(1000,20000), Vector2(86.602, 31.623), "earth", 2, 2)
	
	
	
	
	
	
	
	
	
	
	
	
	

func calcoli():
	# buco nero di massa 150000 con sole a distanza 20000
	var G = 1000
	var M : float = 150000
	var R : float = 20000
	var T : float = 5 * 60
	var PlanetRadius : float = 5 * 1
	
	var res = (2 * PI) * (R ** 1.5) / sqrt(G * M) 
	var vel = sqrt(G * M / R)
	var surface_acceleration = G * M / (PlanetRadius ** 2)
	
	print("              Period = " + str(res) +                  " seconds")
	print("              Period = " + str(res / 60) +             " minutes")
	
	print("                 Vel = " + str(vel) +                  " pixel per second")
	print("surface acceleration = " + str(surface_acceleration) + " pixel per secondo quadrato")
	print("radius to fixed period = " + str(calc_distance(M,T)) + " pixels")
	
	pass

func calcoli_sole_terra():
	# sole di massa 1000 con terra a distanza 1000
	var G = 1000
	var M : float = 1000
	var R : float = 1000
	var T : float = 5 * 60
	var PlanetRadius : float = 5 * 1
	
	var res = (2 * PI) * (R ** 1.5) / sqrt(G * M) 
	var vel = sqrt(G * M / R)
	var surface_acceleration = G * M / (PlanetRadius ** 2)
	
	print("              Period = " + str(res) +                  " seconds")
	print("              Period = " + str(res / 60) +             " minutes")
	
	print("                 Vel = " + str(vel) +                  " pixel per second")
	print("surface acceleration = " + str(surface_acceleration) + " pixel per secondo quadrato")
	
	pass


