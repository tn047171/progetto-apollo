extends CharacterBody2D

class_name CelestialBody

@export var mass : float = 1.0
@export var initial_velocity : Vector2 = Vector2(0.0, 0.0)
@export var sprite_number : int = 1
@export var radius : float = 1

@onready var simulation_unit : SimulationUnit = $SimulationUnit

func _ready():
	simulation_unit.mass = mass
	velocity = initial_velocity
	simulation_unit.position = position
	$AnimatedSprite2D.frame = sprite_number
	rescale()

func _physics_process(delta):
#	apply_central_force(simulation_unit.gravity_force)
	velocity += (simulation_unit.gravity_force / simulation_unit.mass) * delta
	move_and_collide(velocity * delta)
	simulation_unit.position = position


func rescale():
	print($CollisionShape2D)
	if $CollisionShape2D != null and $AnimatedSprite2D != null:
		$CollisionShape2D.scale = Vector2(radius, radius)
		$AnimatedSprite2D.scale = Vector2(radius, radius)

func set_zoom(value):
	$Label.scale = Vector2.ONE / value
	pass

