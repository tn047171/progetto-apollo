extends Node

var GRAVITY_CONSTANT = 1000

var sim_engine = null
var zoom = 1

func set_sim_engine(sim):
	sim_engine = sim

func get_sim_engine():
	assert(sim_engine != null)
	return sim_engine

func set_zoom(_zoom):
	zoom = _zoom

func _process(delta):
	var bodies = %Bodies
	for _i in bodies.get_children():
		_i.set_zoom(zoom)
