extends CharacterBody2D


@onready var simulation_unit : SimulationUnit = $SimulationUnit

var engine_power : float = 80
var engine_level : int = 0

func get_engine_power():
	var p = engine_level * 20 + max(engine_level - 10, 0) * 50 + max(engine_level - 20, 0) * 150
	return p

func _ready():
	simulation_unit.mass = 0.1
	simulation_unit.position = position
	velocity = Vector2(0, -273.86)
	$SpaceshipSprite/EngineFire.play()

var engine_firing = false
# true if the engine has been in the same state (on/off) for a long time (given by engine_long_use_timer)
var engine_long_use = true
@onready var engine_long_use_timer = $SpaceshipSprite/EngineFire/EngineLongUseTimer

func _physics_process(delta):
	velocity += (simulation_unit.gravity_force / simulation_unit.mass) * delta
	move_and_slide()
	look_at(get_global_mouse_position())
	
	if Input.is_action_just_released("ENGINE_UP"):
		engine_level += 1
	if Input.is_action_just_released("ENGINE_DOWN"):
		engine_level -= 1
	engine_level = clampi(engine_level, 0, 30)
	if Input.is_action_pressed("ENGINE_FIRE") and engine_level > 0:
		velocity += Vector2.from_angle(rotation) * get_engine_power() * delta
		$SpaceshipSprite/EngineFire.visible = true
		if not $SpaceshipSprite/EngineFire/EngineAudio.playing:
			$SpaceshipSprite/EngineFire/EngineAudio.play()
		if not engine_firing:
			engine_firing = true
			if engine_long_use:
				$SpaceshipSprite/EngineFire/EngineStartAudio.play()
			engine_long_use = false
			engine_long_use_timer.start()
	else:
		$SpaceshipSprite/EngineFire.visible = false
		if $SpaceshipSprite/EngineFire/EngineAudio.playing:
			$SpaceshipSprite/EngineFire/EngineAudio.stop()
		if engine_firing:
			engine_firing = false
			if engine_long_use:
				$SpaceshipSprite/EngineFire/EngineCutAudio.play()
			engine_long_use = false
			engine_long_use_timer.start()
#		$SpaceshipSprite/EngineFire/EngineAudio.playing = false
			
	
	if Input.is_action_just_released("BREAK"):
		velocity = Vector2.ZERO
	
	simulation_unit.position = position
		
func set_zoom(value):
	$SpaceshipSprite.scale = Vector2.ONE / value


	


func _on_engine_long_use_timer_timeout():
	engine_long_use = true
