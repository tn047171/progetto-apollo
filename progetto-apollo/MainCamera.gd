extends Camera2D

class_name MainCamera

@onready var sim_engine = %Game.get_sim_engine()
@onready var space_ship_sim_unit = $"../Bodies/SpaceShip/SimulationUnit"


func _ready():
	position = space_ship_sim_unit.get_parent().position
	pass


# zoom tweens
@onready var zoom_tween: Tween = null
@onready var position_tween: Tween = null
func _set_zoom_level(value: float) -> void:
	zoom_tween = self.create_tween()
	zoom_tween.tween_property(self, "zoom", Vector2(value, value), 0.1)
func _set_position(value: Vector2) -> void:
	position_tween = self.create_tween()
	position_tween.tween_property(self, "position", value, 0.1)
	
	

func _physics_process(delta):

	# move camera		
	var body_influences = sim_engine.get_body_influences(space_ship_sim_unit)
	if body_influences.size() == 0:
		return
	var current_most_influential_body = body_influences[0]["body"].get_parent()
	var pos = current_most_influential_body.position
	_set_position(pos)
	
	# zoom camera
	var space_ship_pos = space_ship_sim_unit.get_parent().global_position
	
	var viewport_x = get_viewport().size.x / 2
	var viewport_y = get_viewport().size.y / 2
	
#	# non unified zoom
#	var zoom_factor_x = 1/(abs(pos.x-space_ship_pos.x) * 1.5/(viewport_x))
#	var zoom_factor_y = 1/(abs(pos.y-space_ship_pos.y) * 1.5/(viewport_y))
#	var non_unified_zoom_factor = min(zoom_factor_x, zoom_factor_y)

	# unified zoom	
	var min_dimension = min(viewport_x, viewport_y)
	var unified_factor = 1/((pos-space_ship_pos).length() * 1.5/(min_dimension))
	
	var zoom_factor = clampf(unified_factor,0,5)
		
	_set_zoom_level(zoom_factor)

	# set game zoom	
	%Game.set_zoom(self.zoom)
