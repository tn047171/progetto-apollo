extends Node

class_name SimulationUnit

var gravity_force = Vector2.ZERO
var mass : float = 0
var position : Vector2
		
func _ready():
	get_parent().get_parent().get_parent().get_sim_engine().subscribe(self)

func _exit_tree():
	get_parent().get_parent().get_parent().get_sim_engine().unsubscribe(self)

#func _physics_process(delta):
#	self.apply_central_force(gravity_force)
#	print(self.linear_velocity)
#	pass

