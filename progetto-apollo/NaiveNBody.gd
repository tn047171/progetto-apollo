extends Node

@onready var GRAVITY_CONSTANT = %Game.GRAVITY_CONSTANT
var RADIUS_MAPPER = 1

var simulationUnits = []

func subscribe(unit):
	simulationUnits.append(unit)
	
func unsubscribe(unit):
	simulationUnits.erase(unit)

func _ready():
	%Game.set_sim_engine(self)
	
func _physics_process(delta):
#	print("simulating " + str(len(simulationUnits)) + " units")
	for bodyOrigin in simulationUnits:
		var force_accumulator = Vector2.ZERO
		for bodyTarget in simulationUnits:
			if bodyOrigin == bodyTarget:
				continue
			var bodyOrigin_pos = bodyOrigin.position
			var bodyTarget_pos = bodyTarget.position
			var bodyOrigin_mass = bodyOrigin.mass
			var bodyTarget_mass = bodyTarget.mass
			var delta_position = bodyTarget_pos - bodyOrigin_pos
#			print("delta_position = " + str(delta_position) + "from " + str(bodyTarget_pos) + " and " + str(bodyOrigin_pos))
			if delta_position.length() < 0.2:
				print("too close")
				continue
			var direction = delta_position.normalized()
			var distance_squared = (delta_position * RADIUS_MAPPER).length_squared()
			var one_body_force_norm = (GRAVITY_CONSTANT * bodyOrigin_mass * bodyTarget_mass) / distance_squared
			var one_body_force = direction * one_body_force_norm
#			print("force = " + str(one_body_force))
			force_accumulator += one_body_force
		bodyOrigin.gravity_force = force_accumulator

func get_body_influences(bodyOrigin):
	var influence_bodies = []
	for bodyTarget in simulationUnits:
		if bodyOrigin == bodyTarget:
			continue
		var bodyOrigin_pos = bodyOrigin.position
		var bodyTarget_pos = bodyTarget.position
		var bodyOrigin_mass = bodyOrigin.mass
		var bodyTarget_mass = bodyTarget.mass
		var delta_position = bodyTarget_pos - bodyOrigin_pos
		if delta_position.length() < 0.2:
			print("too close")
			continue
		var direction = delta_position.normalized()
		var distance_squared = delta_position.length_squared()
		var one_body_force_norm = (GRAVITY_CONSTANT * bodyOrigin_mass * bodyTarget_mass) / distance_squared
		influence_bodies.append({"body":bodyTarget, "gravity":one_body_force_norm})
	influence_bodies.sort_custom(func(a, b): return a["gravity"] > b["gravity"])
	return influence_bodies
	
